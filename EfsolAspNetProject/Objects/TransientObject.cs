﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfsolAspNetProject.Objects
{
    public class TransientObject : BaseObject, IExampleObject
    {
    }

    public class ScopedObject : BaseObject, IExampleObject
    {
    }

    public class SingletonObject : BaseObject, IExampleObject
    {
    }
}
