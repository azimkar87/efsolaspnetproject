﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfsolAspNetProject.Objects
{
    public interface IExampleObject
    {
        Guid GetGuid();
    }
}
