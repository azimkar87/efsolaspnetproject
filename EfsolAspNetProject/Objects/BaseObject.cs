﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfsolAspNetProject.Objects
{
    public class BaseObject
    {
        protected Guid _guid;

        public BaseObject()
        {
            _guid = Guid.NewGuid();
        }

        public virtual Guid GetGuid()
        {
            return _guid;
        }
    }
}
