﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EfsolAspNetProject.Objects;

namespace EfsolAspNetProject
{
    public class MyServiceClass
    {
        private readonly TransientObject _transientObject;
        private readonly ScopedObject _scopedObject;
        private readonly SingletonObject _singletonObject;

        public MyServiceClass(TransientObject transientObject, ScopedObject scopedObject, SingletonObject singletonObject)
        {
            _transientObject = transientObject;
            _scopedObject = scopedObject;
            _singletonObject = singletonObject;
        }

        public Guid GetTransient()
        {
            return _transientObject.GetGuid();
        }

        public Guid GetScoped()
        {
            return _scopedObject.GetGuid();
        }

        public Guid GetSingleton()
        {
            return _singletonObject.GetGuid();
        }
    }
}
