﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfsolAspNetProject.Models
{
    public class PointOfSale : Entity
    {
        public string Name { get; set; }

        public int CityId { get; set; }
        public City City { get; set; }
    }
}
