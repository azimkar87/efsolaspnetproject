﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfsolAspNetProject.Models
{
    public class City : Entity
    {
        public string Name { get; set; }

        public ICollection<PointOfSale> PointsOfSale { get; set; }

        public City()
        {
            PointsOfSale = new List<PointOfSale>();
        }
    }
}
