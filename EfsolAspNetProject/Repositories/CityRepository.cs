﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EfsolAspNetProject.Models;
using EfsolAspNetProject.Repositories.Contracts;

namespace EfsolAspNetProject.Repositories
{
    public class CityRepository : Repository<City>, ICityRepository
    {
        public CityRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Cities;
        }
    }
}
