﻿using EfsolAspNetProject.Models;

namespace EfsolAspNetProject.Repositories.Contracts
{
    public interface ICityRepository : IRepository<City>
    {

    }
}