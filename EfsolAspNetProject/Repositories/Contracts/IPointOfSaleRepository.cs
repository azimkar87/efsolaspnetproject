﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EfsolAspNetProject.Models;

namespace EfsolAspNetProject.Repositories.Contracts
{
    public interface IPointOfSaleRepository : IRepository<PointOfSale>
    {
    }
}
