﻿using System.Collections.Generic;
using EfsolAspNetProject.Models;

namespace EfsolAspNetProject.Repositories.Contracts
{
    public interface IRepository<T> where T : Entity
    {
        void Add(T entity);
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Update(T entity);
        void Delete(T entity);
    }
}
