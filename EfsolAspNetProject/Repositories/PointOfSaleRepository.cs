﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EfsolAspNetProject.Models;
using EfsolAspNetProject.Repositories.Contracts;

namespace EfsolAspNetProject.Repositories
{
    public class PointOfSaleRepository : Repository<PointOfSale>, IPointOfSaleRepository
    {
        public PointOfSaleRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.PointsOfSale;
        }
    }
}
