﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EfsolAspNetProject.Models;
using EfsolAspNetProject.Objects;

namespace EfsolAspNetProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly TransientObject _transientObject;
        private readonly ScopedObject _scopedObject;
        private readonly SingletonObject _singletonObject;
        private readonly MyServiceClass _myServiceClass;

        public HomeController(
            TransientObject transientObject,
            ScopedObject scopedObject,
            SingletonObject singletonObject,
            MyServiceClass myServiceClass)
        {
            _transientObject = transientObject;
            _scopedObject = scopedObject;
            _singletonObject = singletonObject;
            _myServiceClass = myServiceClass;
        }

        public IActionResult Index()
        {
            ViewBag.TransientObject = _transientObject.GetGuid();
            ViewBag.ScopedObject = _scopedObject.GetGuid();
            ViewBag.SingletonObject = _singletonObject.GetGuid();

            ViewBag.TransientObject2 = _myServiceClass.GetTransient();
            ViewBag.ScopedObject2 = _myServiceClass.GetScoped();
            ViewBag.SingletonObject2 = _myServiceClass.GetSingleton();

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
