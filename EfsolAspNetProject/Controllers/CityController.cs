﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EfsolAspNetProject.Repositories.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace EfsolAspNetProject.Controllers
{
    public class CityController : Controller
    {
        private readonly ICityRepository _cityRepository;

        public CityController(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }

        public IActionResult Index()
        {
            var cities = _cityRepository.GetAll();
            return View(cities);
        }
    }
}