﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EfsolAspNetProject;
using EfsolAspNetProject.Models;
using EfsolAspNetProject.Repositories.Contracts;

namespace EfsolAspNetProject.Controllers
{
    public class PointOfSalesController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly IPointOfSaleRepository _pointOfSaleRepository;

        public PointOfSalesController(IPointOfSaleRepository pointOfSaleRepository)
        {
            _pointOfSaleRepository = pointOfSaleRepository;
        }

        // GET: PointOfSales
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.PointsOfSale.Include(p => p.City);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: PointOfSales/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pointOfSale = await _context.PointsOfSale
                .Include(p => p.City)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (pointOfSale == null)
            {
                return NotFound();
            }

            return View(pointOfSale);
        }

        // GET: PointOfSales/Create
        public IActionResult Create()
        {
            ViewData["CityId"] = new SelectList(_context.Cities, "Id", "Id");
            return View();
        }

        // POST: PointOfSales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,CityId")] PointOfSale pointOfSale)
        {
            if (ModelState.IsValid)
            {
                _context.Add(pointOfSale);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CityId"] = new SelectList(_context.Cities, "Id", "Id", pointOfSale.CityId);
            return View(pointOfSale);
        }

        // GET: PointOfSales/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pointOfSale = await _context.PointsOfSale.FindAsync(id);
            if (pointOfSale == null)
            {
                return NotFound();
            }
            ViewData["CityId"] = new SelectList(_context.Cities, "Id", "Id", pointOfSale.CityId);
            return View(pointOfSale);
        }

        // POST: PointOfSales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,CityId")] PointOfSale pointOfSale)
        {
            if (id != pointOfSale.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(pointOfSale);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PointOfSaleExists(pointOfSale.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CityId"] = new SelectList(_context.Cities, "Id", "Id", pointOfSale.CityId);
            return View(pointOfSale);
        }

        // GET: PointOfSales/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pointOfSale = await _context.PointsOfSale
                .Include(p => p.City)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (pointOfSale == null)
            {
                return NotFound();
            }

            return View(pointOfSale);
        }

        // POST: PointOfSales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var pointOfSale = await _context.PointsOfSale.FindAsync(id);
            _context.PointsOfSale.Remove(pointOfSale);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PointOfSaleExists(int id)
        {
            return _context.PointsOfSale.Any(e => e.Id == id);
        }
    }
}
